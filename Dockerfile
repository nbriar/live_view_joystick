from elixir:1.10

# Install debian packages
RUN apt-get update
RUN apt-get install --yes build-essential inotify-tools postgresql-client

# Install Phoenix packages
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new.ez

# Install node
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs


ENV MIX_ENV prod
WORKDIR /app
# This needs the correct path!
COPY ./_build/prod .
EXPOSE 4000

CMD ["mix", "phx.server"]
