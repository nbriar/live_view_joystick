defmodule Noble.Repo.Migrations.CreateUsers do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :auth0_id, :text, unique: true

      timestamps()
    end
  end
end
