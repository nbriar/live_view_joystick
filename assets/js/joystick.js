import nipplejs from "nipplejs";

let options = {
  // zone: document.getElementById("zone_joystick"),
  color: "orange",
  size: 100,
  multitouch: false,
  mode: "semi"
};

const MovementJoystick = {
  initJoystick() {
    let hook = this,
      manager = nipplejs.create(options);

    manager.on("added", function(evt, nipple) {
      nipple.on("move", function(evt, data) {
        hook.moved(hook, data);
      });
    });

    return manager;
  },

  mounted() {
    this.initJoystick();
  },

  moved(hook, data) {
    const { angle, direction, distance, vector } = data;
    hook.pushEvent("joystick", { angle, direction, distance, vector });
  }
};

export default MovementJoystick;
