import { Socket } from "phoenix"
let socket = new Socket("/socket", { params: { token: window.userToken } })

// if (window.userToken) {
//   socket.connect()

//   // gameToken is the id of the specific game instance
//   let channel = socket.channel("room:" + window.userToken, {})

//   channel.on("finish", payload => {
//     // Do this to clear the results function once the game is over
//     window.Results = (function() {
//       let report = function(user, results) {}

//       return { report: report }
//     })()
//   })

//   channel.join()
//   // for troubleshooting
//   // .receive("ok", resp => { console.log("Joined successfully", resp) })
//   // .receive("error", resp => { console.log("Unable to join", resp) })

//   window.Results = (function() {
//     // Possible inputs for results:
//     // { score: int }
//     // { time: interval (int?) }
//     let report = function(gameToken, results) {
//       if (gameToken) {
//         channel.push("results", { results: results })
//       }
//     }

//     return {
//       report: report
//     }
//   })()
// } else {
//   // We do this to have the function do a null op if the player isn't logged in.
//   window.Results = (function() {
//     let report = function(userToken, results) {}

//     return { report: report }
//   })()
// }

export default socket
