use Mix.Config

config :noble, NobleWeb.Endpoint,
  server: true,
  render_errors: [view: NobleWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Noble.PubSub, adapter: Phoenix.PubSub.PG2],
  cache_static_manifest: "priv/static/cache_manifest.json"

# Do not print debug messages in production
config :logger,
  level: :info,
  backends: [Timber.LoggerBackends.HTTP]

import_config "prod.secret.exs"
