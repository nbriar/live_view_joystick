import Config

# Since phoenix already handles env vars in the production.secret.exs file we put everything in there.
# This file is to let gigalixir know we're using elixir releases for deployment.
# Don't remove or add to this file.
