# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :noble,
  ecto_repos: [Noble.Repo]

# Configures the endpoint
config :noble, NobleWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "PCeD02kyxwiafZox6omGKqVKgOylUAJm2CZpgvDDmIq4QK9ij9xVr+hTuqgj/smV",
  render_errors: [view: NobleWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Noble.PubSub, adapter: Phoenix.PubSub.PG2],
  instrumenters: [Appsignal.Phoenix.Instrumenter]

config :phoenix, :template_engines,
  eex: Appsignal.Phoenix.Template.EExEngine,
  exs: Appsignal.Phoenix.Template.ExsEngine

config :noble, NobleWeb.Gettext, default_locale: "en", locales: ~w(en)

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configures Ueberauth
config :ueberauth, Ueberauth,
  providers: [
    auth0: {Ueberauth.Strategy.Auth0, []}
  ]

# Configures Ueberauth's Auth0 auth provider
config :ueberauth, Ueberauth.Strategy.Auth0.OAuth,
  domain: {:system, "AUTH0_DOMAIN"},
  client_id: {:system, "AUTH0_CLIENT_ID"},
  client_secret: {:system, "AUTH0_CLIENT_SECRET"}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
