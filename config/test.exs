import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :noble, NobleWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :noble, Noble.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "noble_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :noble, Noble.Services.GeoIp,
  api_key: "test",
  url: "http://localhost:9081/geoip"
