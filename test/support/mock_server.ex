defmodule Noble.Test.MockServer do
  @moduledoc false

  use Plug.Router
  alias Noble.Test.MockServerState

  plug Plug.Parsers,
    parsers: [:json],
    pass: ["application/json", "text/xml"],
    json_decoder: Poison

  plug :match
  plug :dispatch

  @doc """
  To use this in your tests you can call:
  Noble.Test.MockServer.set_test_case(:user_settings, :twofa)
  then implement a case statement in the endpoint to get the expected response back
  """
  def set_test_case(endpoint, value) do
    Agent.update(MockServerState, &Map.put(&1, endpoint, value))
  end

  @doc """
  This is used to reset the test state once tests in a module have been performed.
  """
  def reset_test_cases do
    Agent.update(MockServerState, fn _ ->
      %{
        geo_ip: :success
      }
    end)
  end

  def test_case(endpoint) do
    Agent.get(MockServerState, &Map.get(&1, endpoint))
  end

  ############### GEO IP MOCKS ##################################
  get "/geoip/:ip" do
    conn = Plug.Conn.put_resp_header(conn, "content-type", "application/json")

    case test_case(:geo_ip) do
      :success -> success(conn, %{region_code: "UT"})
      :failure -> success(conn, %{region_code: "Unsupported"})
      _ -> success(conn, %{region_code: "UT"})
    end
  end

  #################### HELPER FUNCTIONS ################################
  match _ do
    send_resp(conn, 404, "there's no mock for that route")
  end

  defp success({:xml, conn, body}), do: conn |> Plug.Conn.send_resp(200, body)

  defp success(conn, body) do
    conn
    |> Plug.Conn.send_resp(200, Poison.encode!(body))
  end

  defp failure(conn, message) do
    conn
    |> Plug.Conn.send_resp(422, Poison.encode!(%{error: message}))
  end
end
