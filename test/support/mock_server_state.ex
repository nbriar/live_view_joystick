defmodule Noble.Test.MockServerState do
  @moduledoc """
  Used to set state for mocks in tests.
  """
  def child_spec(_) do
    %{
      id: __MODULE__,
      start:
        {__MODULE__, :start_link,
         [
           %{
             geo_ip: :success
           }
         ]},
      restart: :permanent,
      shutdown: 5000,
      type: :worker
    }
  end

  def start_link(initial_state) do
    Agent.start_link(fn -> initial_state end, name: __MODULE__)
  end
end
