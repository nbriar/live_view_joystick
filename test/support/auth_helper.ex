defmodule NobleWeb.Test.AuthHelper do
  @moduledoc false
  alias Noble.Models.User

  def assign_current_user(conn, nil), do: Plug.Conn.assign(conn, :current_user, nil)
  def assign_current_user(conn, %User{} = user), do: Plug.Conn.assign(conn, :current_user, user)
end
