defmodule Noble.Factory do
  @moduledoc false

  use ExMachina.Ecto, repo: Noble.Repo

  require DateTime

  alias Noble.Models.User

  def user_factory do
    {_, uuid} = Ecto.UUID.load(Ecto.UUID.bingenerate())

    %User{
      id: uuid,
      auth0_id: sequence(:auth0_id, &"auth0|#{&1}"),
      name: sequence(:name, &"John#{&1}"),
      email: sequence(:email, &"john#{&1}@email.com"),
      avatar: sequence(:image_url, &"https://picsum.photos/40/40?#{&1}avatar")
    }
  end
end
