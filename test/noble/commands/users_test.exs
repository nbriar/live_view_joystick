defmodule Noble.Commands.UsersTest do
  @moduledoc false
  use Noble.DataCase
  import Noble.Factory
  alias Noble.Commands.Users

  @auth %Ueberauth.Auth{
    credentials: %Ueberauth.Auth.Credentials{
      expires: true,
      expires_at: 1_561_750_535,
      other: %{
        "id_token" =>
          "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rWkNSak01UkRoRFFqUkRRek5FUmpRd01qVkRRVFl5TkRFM1F6azBNa0pFTURreU5qY3dRUSJ9.eyJuaWNrbmFtZSI6Im5icmlhcmRhdmlzIiwibmFtZSI6Im5icmlhcmRhdmlzQGdtYWlsLmNvbSIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci9hY2FmMzFjZTUxODc0YTg0YTZjNTQzNGY5ODNhODJiMD9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRm5iLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDE5LTA2LTI3VDE5OjM1OjM0Ljg4NVoiLCJlbWFpbCI6Im5icmlhcmRhdmlzQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9icmlhcmFwcC5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NWFkZDYwNTk2N2RkNTYxYWQwYWJlYTgwIiwiYXVkIjoidENYT3pqOXpDRnFjWnExOHZaUEI5eHVGM2d3NGxFQVkiLCJpYXQiOjE1NjE2NjQxMzUsImV4cCI6MTU2MTcwMDEzNX0.EDFFNfCqbHGrVjs8q4RFsvpU_g4acE9u7xoLth-yUMgi_79_Cg7TF-0sjg5nMlpEkTUfolulUoJN5il5wj2sG3pXJCK0K4r_E9fPoOR7bbetGBJ5oQDKu5n4xP9icj0m6t75s6sPp22ywVhbkadWtAyidsKPgF7gKCCVPRmi-xdigkhKQjgAm_v7xzoVCR6yIgPoiVaozB_xoN4Arik4urXp5kOMnkwQ9ukDmEOhgj1zdTb9_YFEGUo7bV_BC4k4jBEJVmLLBB0vAo_0oCA9UEggJxaqaab2BUQAEighsaOUfsa9UbSAtQVXTTneEQeZZxYw1toeg61xND6hY7qXVQ"
      },
      refresh_token: nil,
      scopes: [""],
      secret: nil,
      token: "oYDbRLc2Yzhg8y9Of3lRVAwgA3Hm_WcX",
      token_type: "Bearer"
    },
    extra: %Ueberauth.Auth.Extra{raw_info: %{}},
    info: %Ueberauth.Auth.Info{
      description: nil,
      email: "nbriardavis@gmail.com",
      first_name: nil,
      image:
        "https://s.gravatar.com/avatar/acaf31ce51874a84a6c5434f983a82b0?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fnb.png",
      last_name: nil,
      location: nil,
      name: "nbriardavis@gmail.com",
      nickname: "nbriardavis",
      phone: nil,
      urls: %{}
    },
    provider: :auth0,
    strategy: Ueberauth.Strategy.Auth0,
    uid: "auth0|5add605967dd561ad0abea80"
  }

  test "we can create a user" do
    assert {:ok, user} = Users.create(%{auth0_id: "auth0|someid"})
  end

  test "we get user information from auth" do
    {:ok, user} = Users.create(%{auth0_id: @auth.uid})
    {:ok, info} = Users.from_auth(@auth)

    assert info.id == user.id
    assert info.avatar == @auth.info.image
  end

  test "deletes a player successfully" do
    player = insert(:user)
    {:ok, _} = Users.delete(player)
  end

  ####
  ## due to foreign keys constraints, this will fail at the moment
  ## foreign keys exceptions are not handlet yet
  ###
  # test "deletes a seller successfully" do
  #   seller = insert(:user)
  #   player = insert(:user)

  #   # add player's address
  #   country = insert(:country)
  #   region = insert(:region, country: country)
  #   insert(:address, user_id: player.id, country_id: country.id, region_id: region.id)

  #   # add seller's address
  #   insert(:address, user_id: seller.id, country_id: country.id, region_id: region.id)

  #   # seller adds awards - items to win
  #   award1 = insert(:award, seller_id: seller.id)
  #   award2 = insert(:award, seller_id: seller.id)

  #   # seller opens two tournaments (one for each award)
  #   game = insert(:game)
  #   tournament1 = insert(:tournament, game_id: game.id, award_id: award1.id)
  #   tournament2 = insert(:tournament, game_id: game.id, award_id: award2.id)

  #   # players buys points
  #   insert(:billing, user_id: player.id)
  #   insert(:points_bought, user_id: player.id)
  #   insert(:points_used, user_id: player.id)

  #   # seller also has some points
  #   insert(:billing, user_id: seller.id)
  #   insert(:points_bought, user_id: seller.id)
  #   insert(:points_used, user_id: seller.id)

  #   # player joins both tournaments
  #   insert(:user_tournament, user_id: player.id, tournament_id: tournament1.id)
  #   insert(:user_tournament, user_id: player.id, tournament_id: tournament2.id)

  #   # player won these awards and keeps them in his user_awards table
  #   insert(:user_award, user_id: player.id, award_id: award1.id)
  #   insert(:user_award, user_id: player.id, award_id: award2.id)

  #    assert {:ok, _} = Users.delete(seller)
  # end

  test "updates a user successfully" do
    user = insert(:user)
    user_update = %{auth0_id: "updated"}
    assert {:ok, updated} = Users.update(user, user_update)
    assert updated.auth0_id == user_update.auth0_id
  end

  test "verifies a user changeset successfully" do
    user = insert(:user)

    res = Users.change(user)
    assert res.valid? == true
  end

  test "find a user by id successfully" do
    user = insert(:user)
    res = Users.find(user.id)
    assert res.id == user.id
  end
end
