defmodule Noble.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, args) do
    children =
      case args do
        [env: :test] -> supervised_children(:test)
        [_] -> supervised_children(:default)
      end

    :telemetry.attach(
      "appsignal-ecto",
      [:noble, :repo, :query],
      &Appsignal.Ecto.handle_event/4,
      nil
    )

    opts = [strategy: :one_for_one, name: Noble.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def supervised_children(:test) do
    supervised_children(:default) ++
      [
        {Noble.Test.MockServerState, [%{}]},
        {Plug.Cowboy, scheme: :http, plug: Noble.Test.MockServer, options: [port: 9081]}
      ]
  end

  def supervised_children(_) do
    [
      Noble.Repo,
      NobleWeb.Endpoint
    ]
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    NobleWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
