defmodule Noble.Models.User do
  @moduledoc """
  User is the model for an individual person.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "users" do
    field :auth0_id, :string
    field :name, :string, virtual: true
    field :email, :string, virtual: true
    field :avatar, :string, virtual: true
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:auth0_id])
    |> validate_required([:auth0_id])
    |> unique_constraint(:auth0_id)
  end
end
