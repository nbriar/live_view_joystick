defmodule Noble.Repo do
  use Ecto.Repo,
    otp_app: :noble,
    adapter: Ecto.Adapters.Postgres
end
