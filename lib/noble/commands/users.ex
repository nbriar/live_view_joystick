defmodule Noble.Commands.Users do
  @moduledoc """
  The User commands.
  """
  alias Noble.Models.User, as: Model
  alias Noble.Repo
  alias Ueberauth.Auth

  @doc """
  Returns the list of users.
  """
  def all do
    Repo.all(Model)
  end

  @doc """
  Gets a single user.
  """
  def find(%{auth0_id: id}), do: Repo.get_by(Model, auth0_id: id)
  def find(id), do: Repo.get(Model, id)

  @doc """
  Creates a user.
  """
  def create(attrs \\ %{}) do
    %Model{}
    |> Model.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.
  """
  def update(%Model{} = user, attrs) do
    user
    |> Model.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.
  """
  def delete(%Model{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.
  """
  def change(%Model{} = user) do
    Model.changeset(user, %{})
  end

  def from_auth(%Auth{} = auth) do
    case find(%{auth0_id: auth.uid}) do
      nil ->
        {:ok, user} = create(%{auth0_id: auth.uid})
        {:ok, basic_info(user, auth)}

      user ->
        {:ok, basic_info(user, auth)}
    end
  end

  defp avatar_from_auth(%{info: %{urls: %{avatar_url: image}}}), do: image
  defp avatar_from_auth(%{info: %{image: image}}), do: image

  defp avatar_from_auth(_auth) do
    "/images/default-profile-pic.png"
  end

  defp basic_info(%Model{} = user, %Auth{} = auth) do
    %Model{
      id: user.id,
      auth0_id: auth.uid,
      name: name_from_auth(auth),
      avatar: avatar_from_auth(auth)
    }
  end

  defp name_from_auth(auth) do
    if auth.info.name do
      auth.info.name
    else
      name =
        [auth.info.first_name, auth.info.last_name]
        |> Enum.filter(&(&1 != nil and &1 != ""))

      if Enum.empty?(name) do
        auth.info.nickname
      else
        Enum.join(name, " ")
      end
    end
  end
end
