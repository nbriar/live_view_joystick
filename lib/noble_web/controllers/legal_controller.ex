defmodule NobleWeb.LegalController do
  @moduledoc false

  use NobleWeb, :controller

  def index(conn, params) do
    render(conn, "#{params["doc"]}.html")
  end
end
