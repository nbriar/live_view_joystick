defmodule NobleWeb.Plugs.Locale do
  @moduledoc false

  import Plug.Conn

  @locales Gettext.known_locales(NobleWeb.Gettext)
  def init(_opts), do: nil

  def call(conn, _opts) do
    case locale_from_params(conn) || locale_from_session(conn) || locale_from_header(conn) do
      nil ->
        locale = Gettext.get_locale()
        conn |> persist_locale(locale)

      locale ->
        Gettext.put_locale(NobleWeb.Gettext, locale)
        conn |> persist_locale(locale)
    end
  end

  defp locale_from_header(conn) do
    conn
    |> extract_accept_language
    |> Enum.find(nil, fn accepted_locale -> Enum.member?(@locales, accepted_locale) end)
  end

  defp locale_from_params(conn) do
    case conn.params["locale"] do
      nil -> nil
      locale -> validate_locale(locale)
    end
  end

  defp locale_from_session(conn) do
    locale = get_session(conn, :locale)
    validate_locale(locale)
  end

  defp persist_locale(conn, new_locale) do
    if locale_from_session(conn) != new_locale do
      conn |> put_session(:locale, new_locale)
    else
      conn
    end
  end

  defp validate_locale(locale) when locale in @locales, do: locale

  defp validate_locale(_locale), do: nil

  def extract_accept_language(conn) do
    case Plug.Conn.get_req_header(conn, "accept-language") do
      [value | _] ->
        value
        |> String.split(",")
        |> Enum.map(&parse_language_option/1)
        |> Enum.sort(&(&1.quality > &2.quality))
        |> Enum.map(& &1.tag)
        |> Enum.reject(&is_nil/1)
        |> ensure_language_fallbacks()

      _ ->
        []
    end
  end

  defp parse_language_option(string) do
    captures = Regex.named_captures(~r/^\s?(?<tag>[\w\-]+)(?:;q=(?<quality>[\d\.]+))?$/i, string)

    quality =
      case Float.parse(captures["quality"] || "1.0") do
        {val, _} -> val
        _ -> 1.0
      end

    %{tag: captures["tag"], quality: quality}
  end

  defp ensure_language_fallbacks(tags) do
    Enum.flat_map(tags, fn tag ->
      [language | _] = String.split(tag, "-")
      if Enum.member?(tags, language), do: [tag], else: [tag, language]
    end)
  end
end
