defmodule NobleWeb.Plugs.StaticPlay do
  @moduledoc """
  Use to add static asset games
  """
  use Plug.Builder

  plug Plug.Static,
    at: "/play/crazy-sheep",
    from: "priv/static/play/crazy-sheep",
    gzip: false

  def init(opts) do
    opts
  end
end
