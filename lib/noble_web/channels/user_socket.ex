defmodule NobleWeb.UserSocket do
  @moduledoc false

  use Phoenix.Socket

  ## Channels
  channel "room:*", NobleWeb.RoomChannel do
    use Phoenix.Channel
  end

  def connect(%{"token" => token}, socket, _connect_info) do
    # max_age: 1209600 is equivalent to two weeks in seconds
    case Phoenix.Token.verify(socket, salt(), token, max_age: 1_209_600) do
      {:ok, user_id} ->
        {:ok, assign(socket, :current_user, user_id)}

      {:error, _reason} ->
        :error
    end
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     NobleWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil

  defp salt do
    Application.get_env(:noble, NobleWeb.Endpoint)[:secret_key_base]
  end
end
