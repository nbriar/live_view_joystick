defmodule NobleWeb.JoystickLiveView do
  @moduledoc false
  use Phoenix.LiveView
  alias NobleWeb.AvatarLiveComponent
  alias NobleWeb.JoystickLiveComponent
  require Logger

  # send or send_update does not work for this component
  # so will render Avatar from this view as a liveComponent
  def render(assigns) do
    ~L"""
    <%= live_component @socket, JoystickLiveComponent, id: :joystick  %>
    <%= live_component @socket, AvatarLiveComponent, id: :avatar, move_params: @move_params %>
    """
  end

  def mount(_params, _session, socket) do
    {:ok, assign_new(socket, :move_params, fn -> %{} end)}
  end

  def handle_event("joystick", move_params, socket) do
    # distance = move_params["distance"]
    # message = {:move, distance: distance}
    # send({:name, NobleWeb.AvatarLiveView}, message)    # NobleWeb.AvatarLiveView.set_position(move_params)
    Logger.info("Avatar position: #{inspect(move_params)}")
    send_update(AvatarLiveComponent, id: :avatar, move_params: move_params)

    {:noreply, socket}
  end
end
