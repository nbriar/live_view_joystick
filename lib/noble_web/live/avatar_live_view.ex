defmodule NobleWeb.AvatarLiveView do
  @moduledoc """
  Eventually we want to have seperated views/components for each live view element
  some a reason send or send_update does not work here - needs more debugging
  """
  use Phoenix.LiveView
  alias NobleWeb.AvatarLiveComponent
  require Logger

  def render(assigns) do
    ~L"""
    <%= live_component @socket, AvatarLiveComponent, id: :avatar, distance: @distance %>
    """
  end

  # def update(assigns, socket) do
  #   IO.inspect("UPDATING")
  # end

  def mount(_params, _session, socket) do
    {:ok, assign_new(socket, :distance, fn -> 0 end)}
  end

  def set_position(move_params) do
    Logger.info("Avatar position: #{inspect(move_params)}")
    # angle = move_params["direction"]["angle"]
    # x = move_params["direction"]["x"]
    # y = move_params["direction"]["y"]
    distance = move_params["distance"]

    send_update(AvatarLiveComponent, id: :avatar, distance: distance)

    # message = {:move, distance: distance}

    # send({:name, NobleWeb.AvatarLiveView}, message)
    # |> IO.inspect()
  end

  def handle_info({:move, [distance: distance]}, socket) do
    Logger.info("Moving")
    send_update(AvatarLiveComponent, id: :avatar, distance: distance)
    {:noreply, assign(socket, distance: distance)}
  end

  # def handle_info(:move, socket) do
  #   # update the list of cards in the socket
  #   IO.puts("MOVING")
  #   IO.inspect(socket)

  #   {:noreply, socket}
  # end

  # defp set_position(socket) do
  #   assign(socket,
  #     top: get_top(socket.assigns),
  #     left: get_left(socket.assigns)
  #   )
  # end

  # defp get_top(assigns) do
  #   cond do
  #     is_integer(assigns["top"]) -> assigns["top"] + 10
  #     assigns["top"] == nil -> 50
  #     true -> 50
  #   end
  # end

  # defp get_left(assigns) do
  #   cond do
  #     is_integer(assigns["left"]) -> assigns["left"] + 10
  #     assigns["left"] == nil -> 50
  #     true -> 50
  #   end
  # end
end
