defmodule NobleWeb.GameLiveView do
  @moduledoc false
  use Phoenix.LiveView

  def render(assigns) do
    ~L"""
    <div style="color: purple">
      Game View
    </div>
    """
  end

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def handle_info(:tick, socket) do
    {:noreply, socket}
  end
end
