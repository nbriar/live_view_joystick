defmodule NobleWeb.JoystickLiveComponent do
  @moduledoc false
  use Phoenix.LiveComponent

  def render(assigns) do
    ~L"""
    <div phx-hook="MovementJoystick">
    </div>
    """
  end
end
