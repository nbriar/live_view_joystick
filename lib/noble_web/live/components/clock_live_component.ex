defmodule NobleWeb.ClockLiveComponent do
  @moduledoc false
  use Phoenix.LiveComponent
  import Calendar.Strftime

  def render(assigns) do
    ~L"""
    <div>
      <h2><%= strftime!(@date, "%r") %></h2>
    </div>
    """
  end
end
