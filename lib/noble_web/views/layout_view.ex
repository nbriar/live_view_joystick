defmodule NobleWeb.LayoutView do
  use NobleWeb, :view

  def top_navigation do
    "top_navigation.html"
  end

  def drawer_menu do
    "drawer_menu.html"
  end
end
