defmodule NobleWeb.Router do
  use NobleWeb, :router
  require Ueberauth
  require Logger

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug NobleWeb.Plugs.Locale
    plug :session_to_assigns
    plug :put_user_token
    plug :protect_access
  end

  pipeline :authenticated do
    plug :secure
  end

  pipeline :protect_access do
    plug :require_header
  end

  scope "/", NobleWeb do
    pipe_through [:browser, :authenticated]
  end

  scope "/", NobleWeb do
    pipe_through :browser

    get "/", HomeController, :index

    get "/logout", AuthController, :logout
  end

  scope "/legal", NobleWeb do
    pipe_through :browser

    get "/:doc", LegalController, :index
  end

  scope "/auth", NobleWeb do
    pipe_through :browser

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
    post "/:provider/callback", AuthController, :callback
    post "/logout", AuthController, :delete
  end

  defp session_to_assigns(conn, _params) do
    user = get_session(conn, :current_user)

    conn
    |> assign(:current_user, user)
  end

  defp secure(conn, _params) do
    user = get_session(conn, :current_user)

    case user do
      nil ->
        conn |> redirect(to: "/auth/auth0") |> halt

      _ ->
        conn
        |> assign(:current_user, user)
    end
  end

  defp put_user_token(conn, _) do
    if current_user = conn.assigns[:current_user] do
      salt = Application.get_env(:noble, NobleWeb.Endpoint)[:secret_key_base]
      token = Phoenix.Token.sign(conn, salt, current_user.id)
      assign(conn, :user_token, token)
    else
      conn
    end
  end

  defp require_header(conn, _) do
    if conn.host =~ "gigalixirapp.com" do
      case(get_req_header(conn, "x-allow-access")) do
        [allow_access] when allow_access == "true" ->
          conn

        _ ->
          Logger.info(
            "A request from " <>
              to_string(:inet_parse.ntoa(conn.remote_ip)) <> " has no x-allow-access header"
          )

          conn |> redirect(to: "/") |> halt()
      end
    else
      conn
    end
  end
end
