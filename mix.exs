defmodule Noble.MixProject do
  @moduledoc false

  use Mix.Project

  def project do
    [
      app: :noble,
      version: "0.1.0",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: [
        noble: [
          include_executables_for: [:unix],
          applications: [runtime_tools: :permanent]
        ]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Noble.Application, [env: Mix.env()]},
      extra_applications: extra_applications(Mix.env())
    ]
  end

  defp extra_applications(:test), do: extra_applications(:default) ++ [:cowboy, :plug]
  defp extra_applications(_), do: [:logger, :runtime_tools, :ueberauth, :ueberauth_auth0]

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:appsignal, "~> 1.12.1"},
      {:calendar, "~> 1.0.0"},
      {:cors_plug, "~> 2.0"},
      {:credo, "~> 1.3.1", [only: [:dev, :test], runtime: false]},
      {:dialyxir, "~> 1.0.0", [only: [:dev, :test], runtime: false]},
      {:ecto_network, "~> 1.3.0"},
      {:ecto_sql, "~> 3.3.4"},
      {:ex_doc, "~> 0.21.3", only: :dev, runtime: false},
      {:ex_machina, "~> 2.4.0", only: :test},
      {:excoveralls, "~> 0.12.3", only: :test},
      {:floki, ">= 0.0.0", only: :test},
      {:gettext, "~> 0.17.4"},
      {:httpoison, "~> 1.6.2"},
      {:jason, "~> 1.2.0"},
      {:phoenix, "~> 1.4.16"},
      {:phoenix_ecto, "~> 4.1.0"},
      {:phoenix_html, "~> 2.14"},
      {:phoenix_live_reload, "~> 1.2.1", only: :dev},
      {:phoenix_live_view, "~> 0.10.0"},
      {:phoenix_pubsub, "~> 1.1.2"},
      {:plug_cowboy, "~> 2.1.2"},
      {:poison, "~> 4.0.1", override: true},
      {:postgrex, ">= 0.0.0"},
      {:remote_ip, "~> 0.2.1"},
      {:set_locale, "~> 0.2.8"},
      {:timber, "~> 3.1.2"},
      {:timber_phoenix, "~> 1.1.0"},
      {:timex, "~> 3.6.1"},
      {:ueberauth, "~> 0.6.3"},
      {:ueberauth_auth0, "~> 0.4.0"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
